<?php

class Apinew{


// Function to make HTTP requests using cURL
function makeRequest($method, $cheurl, $data = null)
{
    $url = 'https://jsonplaceholder.typicode.com'.$cheurl;

    

    // Initialize cURL session
    $ch = curl_init($url);

    // Set cURL options
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);

    // If there is data to send, set additional cURL options
    if ($data !== null) {
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    }

    // Execute cURL session and get the response
    $response = curl_exec($ch);

    // Check for cURL errors
    if (curl_errno($ch)) {
        echo 'Curl error: ' . curl_error($ch);
        // Handle the error as needed
    } else {
        // Decode the JSON response
        $result = json_decode($response, true);

        // Output the decoded data
        // print_r($result);
        return($result);
    }

    // Close cURL session
    curl_close($ch);
}
}

?>
